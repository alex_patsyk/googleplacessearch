import React, { Component } from 'react'
import { Button, Container, Row, Col, Form, Table, ButtonGroup, ListGroup } from 'react-bootstrap';
import Header from './Header';
import Map from './Map';
import MapIcon from '../images/map-marked-alt-solid.svg';
import ExternalLinkIcon from '../images/external-link-alt-solid.svg';
import TrashIcon from '../images/trash-alt-solid.svg';
import Loader from '../images/loading.gif';

interface State {
  searchText: string,
  emtyResponse: boolean,
  places: {
    html_attributions: Array<any> | null,
    results: Array<any>,
    status: string
  },
  savedPlaces: Array<{
    _id: string,
    id: string,
    name: string,
    place_id: string,
    address: string,
    lat: string,
    lng: string
  }>,
  modalShow: boolean,
  modalData: {
    placeName: string,
    lat: string,
    lng: string,
  },
  isLoading: boolean,
}

export default class App extends Component {
  constructor(props: any) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeSearchText = this.onChangeSearchText.bind(this);
    this.openModal = this.openModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  state: State = {
    searchText: '',
    emtyResponse: false,
    places: {
      html_attributions: null,
      results: [],
      status: '',
    },
    savedPlaces: [],
    modalShow: false,
    modalData: {
      placeName: '',
      lat: '',
      lng: '',
    },
    isLoading: false,
  }

  componentDidMount() {
    this.showLoader()
    fetch('/places')
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(data => {
        this.setState({ savedPlaces: data });
        this.showLoader()
      })
      .catch(error => {
        this.showLoader()
        window.alert(`${error} please refresh page or try later`)
        console.log("error", error);
      });
  }

  onChangeSearchText(e: any) {
    e.preventDefault();
    this.setState({ searchText: e.target.value })
  }

  onSubmit(e: any): void {
    e.preventDefault();
    this.showLoader()
    fetch('/places/findplace', {
      method: "POST",
      body: JSON.stringify({ searchText: encodeURI(this.state.searchText) }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(data => {
        if (data.status === "OK") {
          this.setState({ places: data });          
        } else {
          this.setState({ emtyResponse: true })
        }
        this.showLoader()
      })
      .catch(error => {
        this.showLoader()
        window.alert(`${error} please refresh page or try later`)
        console.log("error", error);
      });
  }

  savePlace(index: number): void {
    const { places: { results }, savedPlaces } = this.state
    const data = results[index]    
    const dataToSave = {
      id: data.id,
      name: data.name,
      place_id: data.place_id,
      address: data.formatted_address,
      lat: data.geometry.location.lat,
      lng: data.geometry.location.lng
    }

    this.showLoader()
    fetch('/places', {
      method: "POST",
      body: JSON.stringify(dataToSave),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(data => {
        if (data.message) {
          window.alert(data.message)
        } else {
          this.setState({ savedPlaces: [...savedPlaces, data] });
        }
        this.showLoader()
      })
      .catch(error => {
        this.showLoader()
        window.alert(`${error} please refresh page or try later`)
        console.log("error", error);
      });
  }

  removePlace(index: number, id: string): void {    
    const isRemove = window.confirm("Are you sure remove this place?");
    if (!isRemove) return;

    this.showLoader()
    fetch(`/places/${id}`, { method: "DELETE" })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(data => {
        if (data.status === "200") {
          this.setState({ savedPlaces: this.state.savedPlaces.filter(item => item._id !== id) });
          this.showLoader()
        }
      })
      .catch(error => {
        this.showLoader()
        window.alert(`${error} please refresh page or try later`)
        console.log("error", error);
      });
  }

  openModal(id: string): void {
    const onePlace = this.state.savedPlaces.filter(item => item.id === id)    
    this.setState({
      modalShow: true,
      modalData: {
        placeName: onePlace[0].name,
        lat: onePlace[0].lat,
        lng: onePlace[0].lng,
      }
    });
  }

  hideModal(): void {
    this.setState({ modalShow: false });
  }

  showLoader(): void {
    this.setState({ isLoading: !this.state.isLoading });
  }

  render() {
    const { places: { results }, savedPlaces, modalShow, modalData, isLoading } = this.state;    
    return (
      <div className="app">
        {isLoading && <div className="overlay"><img src={Loader} alt="Loading"/></div>}
        <Header />
        <Container>
          <Row>
            <Col sm={4}>
              <Form onSubmit={this.onSubmit}>
                <Row>
                  <Col>
                    <Form.Control type="text" placeholder="Enter address" value={this.state.searchText} onChange={this.onChangeSearchText} />
                  </Col>
                  <Col xs={'auto'}>
                    <Button variant="primary" type="submit">Search</Button>
                  </Col>
                </Row>
              </Form>
              <div className="results">
                <ListGroup className="mt-3">
                  {results.length > 0 && results.map((place, index) => (
                    <ListGroup.Item key={place.id}>
                      <Row className="align-items-center">
                        <Col className="results__title">         
                          <img className="results__icon" src={place.icon} alt={place.name} />                          
                          <span>{place.name}</span>
                        </Col>
                        <Col xs={'auto'}>
                          <Button
                            variant="success"
                            onClick={() => this.savePlace(index)}
                          >
                            Save
                          </Button>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              </div>
            </Col>
            <Col sm={8}>
              <Table striped bordered hover responsive>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {savedPlaces.length > 0 && savedPlaces.map((place, index) => (
                    <tr key={place.id}>
                      <td>{place.name}</td>
                      <td>{place.address}</td>
                      <td>
                        <ButtonGroup size="lg" className="mb-2">
                          <Button variant="link" onClick={() => this.openModal(place.id)}>
                            <img className="action-icon" src={MapIcon} alt="Open map" />
                          </Button>
                          <a
                            className="btn btn-link"
                            href={`https://www.google.com/maps/search/?api=1&query=${place.lat},${place.lng}&query_place_id=${place.place_id}`}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <img className="action-icon action-icon--middle" src={ExternalLinkIcon} alt="Open map" />
                          </a>
                          <Button variant="link" onClick={() => this.removePlace(index, place._id)}><img className="action-icon" src={TrashIcon} alt="Remove place" /></Button>
                        </ButtonGroup>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
        <Map
          show={modalShow}
          modalData={modalData}
          hideModal={this.hideModal}
        />
      </div>
    );
  }
}
