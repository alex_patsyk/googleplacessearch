import React, { Component } from "react"
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import Button from "react-bootstrap/Button"
import Modal from "react-bootstrap/Modal"
import ModalBody from "react-bootstrap/ModalBody"
import ModalHeader from "react-bootstrap/ModalHeader"
import ModalFooter from "react-bootstrap/ModalFooter"
import ModalTitle from "react-bootstrap/ModalTitle"

interface Props {
    show: boolean,
    hideModal: any,
    modalData: {
        placeName: string,
        lat: string,
        lng: string,
    }
}

export default class Map extends Component<Props> {
    render(){
        const { show, hideModal, modalData } = this.props
        const GoogleMapHOC = withGoogleMap(props => (
            <GoogleMap
              defaultCenter = {{ lat: Number(modalData.lat), lng: Number(modalData.lng) }}
              defaultZoom = { 13 }              
            >
                <Marker position={{ lat: Number(modalData.lat), lng: Number(modalData.lng) }} />
            </GoogleMap>
         ))

        return(
            <Modal
                show={show}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered                
                >
                <ModalHeader>
                    <ModalTitle id="contained-modal-title-vcenter">
                        {modalData.placeName}
                    </ModalTitle>
                </ModalHeader>
                <ModalBody>                    
                    <GoogleMapHOC
                        containerElement={ <div style={{ height: `500px`, width: '100%' }} /> }
                        mapElement={ <div style={{ height: `100%` }} /> }
                    />                    
                </ModalBody>
                <ModalFooter>
                    <Button onClick={hideModal}>Close</Button>
                </ModalFooter>
                </Modal>
        )
    }
}