import React from 'react'

const Header = () => (
    <header className="header">
        <h2>Places Search</h2>
    </header>
)

export default Header