This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the "/src" and "/backend" directories, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Back-end will work on http://localhost:4000.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
