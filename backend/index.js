require('dotenv/config')

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const app =  express()

//Connect to db
mongoose.connect(
  process.env.DB_CONNECTION,
  { 
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  () => {
    console.log('connected to DB')
  })

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cors())

// import routes
const placesRoutes = require('./routes/places')
app.use('/places', placesRoutes)

app.get('/', (req, res) => {  
  res.json({status: 'Home'})  
})

// 404 Error
app.use((req, res) => {
  res.json({status: '404'})  
});

app.listen(4000)