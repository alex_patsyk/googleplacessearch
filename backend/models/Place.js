const mongoose = require('mongoose')

const PlaceSchema = mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    place_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    lat: {
        type: Number,
        required: true,
    },
    lng: {
        type: Number,
        required: true,
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Places', PlaceSchema)