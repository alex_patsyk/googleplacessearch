const express = require('express')
const fetch = require('node-fetch')
const Place = require('../models/Place')
const router = express.Router()

router.get('/', async(req, res) => {
    try {
        const place = await Place.find()
        res.json(place)
    } catch (err) {
        res.json({ message: err })
    }
})

router.post('/findplace', async(req, res) => {
    const url = `https://maps.googleapis.com/maps/api/place/textsearch/json?input=${req.body.searchText}&inputtype=textquery&key=${process.env.GOOGLE_MAPS_KEY}`;
    try {
        const response = await fetch(url);
        const place = await response.json();
        res.json(place)
    } catch (err) {
        res.json({ message: err })
    }
})

router.post('/', async(req, res) => {
    const place = new Place({
        id: req.body.id,
        name: req.body.name,
        place_id: req.body.place_id,
        address: req.body.address,
        lat: req.body.lat,
        lng: req.body.lng,
    })

    try {
        const findPlace = await Place.find({ id: req.body.id })
        if (findPlace.length > 0) {
            res.json({ message: `Place ${req.body.name} already exists` })
        } else {
            const savedPlace = await place.save()
            res.json(savedPlace)
        }
    } catch (err) {
        res.json({ message: err })
    }
})

router.delete('/:placeId', async(req, res) => {
    try {
        const removedPlace = await Place.remove({ _id: req.params.placeId })
        if (removedPlace) {
            res.json({ status: '200' })
        }
    } catch (err) {
        res.json({ message: err })
    }
})


module.exports = router